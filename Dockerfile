FROM php:7.0.17-apache

RUN apt-get update
RUN apt-get install -y apt-utils vim mariadb-client
ADD ./php.ini /usr/local/etc/php/

EXPOSE 80
